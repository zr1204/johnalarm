package com.example.johnproject

import android.app.AlarmManager
import android.app.AlarmManager.INTERVAL_DAY
import android.app.PendingIntent
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.example.johnproject.MyConstant.EVERYDAY
import com.example.johnproject.MyConstant.ONETIME
import java.text.SimpleDateFormat
import java.util.*

class CustomAlarmManager {


    var alarmManager: AlarmManager? = null
    var context: Context? = null

    constructor(_context: Context) {

        this.context = _context;

        alarmManager = context!!.getSystemService(ALARM_SERVICE) as AlarmManager

    }


    fun fireDailyAlarm(alarm: Alarm, requestCode: Int) {


        val timeToTrigger: Long = getTimeToTrigger(EVERYDAY, alarm.milliseconds!!);

        Log.e("fireDailyAlarm", "request code: $requestCode , timeToTrigger: $timeToTrigger  ")


        val pendingIntent = createPendingIntent(alarm, timeToTrigger, EVERYDAY, true, requestCode)

        alarmManager!!.setRepeating(AlarmManager.RTC_WAKEUP, timeToTrigger, INTERVAL_DAY, pendingIntent)


    }

    fun fireOneTimeAlarm(alarm: Alarm, requestCode: Int) {

        val timeToTrigger: Long = getTimeToTrigger(ONETIME, alarm.milliseconds!!);

        Log.e("fireOneTimeAlarm", "request code: $requestCode , timeToTrigger: $timeToTrigger  ")


        val pendingIntent = createPendingIntent(alarm, timeToTrigger, ONETIME, false, requestCode)

        alarmManager!!.set(AlarmManager.RTC_WAKEUP, timeToTrigger, pendingIntent)

    }

    fun fireWeeklyAlarm(alarm: Alarm, requestCode: Int, dayOfWeek: Int) {


        val timeToTrigger: Long = getTimeToTrigger(dayOfWeek, alarm.milliseconds!!);

        Log.e("fireWeeklyAlarm", "request code: $requestCode , timeToTrigger: $timeToTrigger")


        val pendingIntent = createPendingIntent(alarm, timeToTrigger, dayOfWeek, true, requestCode)


        alarmManager!!.setRepeating(AlarmManager.RTC_WAKEUP, timeToTrigger, INTERVAL_DAY * 7, pendingIntent)

    }


    fun fireSnoozeAlarm(alarm: Alarm) {


        val pendingIntent = createPendingIntent(alarm, alarm.milliseconds!!, ONETIME, false, alarm.id!!.toInt())

        alarmManager!!.set(AlarmManager.RTC_WAKEUP, alarm.milliseconds!!, pendingIntent)

    }

    fun checkAlarmAvailable(requestCode: Int) {

        val intent2 = Intent(context, AlarmActivity::class.java)
        intent2.flags = Intent.FLAG_ACTIVITY_NEW_TASK


        val isWorking = (PendingIntent.getActivity(context, requestCode, intent2, PendingIntent.FLAG_NO_CREATE) != null)

        Toast.makeText(context, "alarm $requestCode is " + (if (isWorking) "" else "not ") + "working...", Toast.LENGTH_SHORT).show()

    }

    fun dismissAlarm(requestCode: Int) {

        Log.e("dismissAlarm", "request code: $requestCode ")


        val intent = Intent(context, AlarmActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        val pendingIntent2 = PendingIntent.getActivity(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        alarmManager!!.cancel(pendingIntent2);

    }

    private fun getTimeToTrigger(dayOfWeek: Int, milliseconds: Long): Long {

        Log.e("getTimeToTrigger", "Before, ${getStringDate(milliseconds)} , dateOfWeek $dayOfWeek")

        val calendar: Calendar = Calendar.getInstance()


        val currentTime = calendar.timeInMillis;


        calendar.timeInMillis = milliseconds
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);


        //just for weekly alarm
        if (dayOfWeek != EVERYDAY && dayOfWeek != ONETIME) {
            calendar.set(Calendar.DAY_OF_WEEK, dayOfWeek + 1);
            Log.e("after set day of week", "${getStringDate(calendar.timeInMillis)}")

        }

        //If past time
        val diff: Long = currentTime - calendar.timeInMillis
        val isPastTime: Boolean = diff > 0;

        if (isPastTime) {

            Log.e("isPastTime", "true")

            if (dayOfWeek == EVERYDAY || dayOfWeek == ONETIME) {
                calendar.add(Calendar.DATE, 1)
                Log.e("add", "7 days")


            } else {
                calendar.add(Calendar.DATE, 7)
                Log.e("add", "7 days")

            }

        } else {
            Log.e("isPastTime", "false")
        }

        Log.e("getTimeToTrigger", "After, ${getStringDate(calendar.timeInMillis)}")


        return calendar.timeInMillis;

    }


    private fun getStringDate(milliseconds: Long): String {
        val currentDate = Date(milliseconds)

        val dfDate = SimpleDateFormat("dd MMM yyyy hh.mm a")

        return dfDate.format(currentDate);

    }


//    private fun getTimeToTrigger(dayOfWeek: Int, milliseconds: Long): Long {
//
//        Log.e("getTimeToTrigger", "Before, $milliseconds")
//
//
//        val calendar: Calendar = Calendar.getInstance()
//        val currentTime = calendar.timeInMillis;
//
//
//        calendar.timeInMillis = milliseconds
//        calendar.set(Calendar.SECOND, 0);
//        calendar.set(Calendar.MILLISECOND, 0);
//
//
//        //just for weekly alarm
////        if (dayOfWeek != EVERYDAY && dayOfWeek != ONETIME) {
////            calendar.set(Calendar.DAY_OF_WEEK, dayOfWeek);
////        }
//
//        //If past time
//        val diff: Long = currentTime - calendar.timeInMillis
//        val isPastTime: Boolean = diff > 0;
//
//        if (isPastTime) {
//
//            Log.e("isPastTime", "true")
//
//
//            if (dayOfWeek == EVERYDAY || dayOfWeek == ONETIME) {
//                calendar.add(Calendar.DATE, 1)
//                Log.e("add", "7 days")
//
//
//            } else {
//                calendar.add(Calendar.DATE, 7)
//                Log.e("add", "7 days")
//
//
//            }
//
//        }else{
//            Log.e("isPastTime", "false")
//        }
//
//        Log.e("getTimeToTrigger", "After, $milliseconds")
//
//
//        return calendar.timeInMillis;
//
//    }

    private fun createPendingIntent(alarm: Alarm,
                                    timeToTrigger: Long,
                                    dayOfWeek: Int?,
                                    isRepeating: Boolean,
                                    requestCode: Int
    ): PendingIntent? {

        val intent = Intent(context, AlarmBroadcastReceiver::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK


        intent.putExtra("requestCode", requestCode)
        intent.putExtra("time", timeToTrigger)
        intent.putExtra("snooze", alarm.snooze)
        intent.putExtra("repeatOn", dayOfWeek)
        intent.putExtra("isRepeating", isRepeating)

        when (alarm.maxSnooze) {
            MaxSnoozesOptions.UNLIMITED.ordinal -> {
                intent.putExtra("maxSnooze", 9999)
            }
            MaxSnoozesOptions.ONE_TIME.ordinal -> {
                intent.putExtra("maxSnooze", 1)
            }
            MaxSnoozesOptions.TWO_TIMES.ordinal -> {
                intent.putExtra("maxSnooze", 2)
            }
            MaxSnoozesOptions.THREE_TIMES.ordinal -> {
                intent.putExtra("maxSnooze", 3)
            }
            MaxSnoozesOptions.FOUR_TIMES.ordinal -> {
                intent.putExtra("maxSnooze", 4)
            }
            MaxSnoozesOptions.FIVE_TIMES.ordinal -> {
                intent.putExtra("maxSnooze", 5)
            }
        }

        if (alarm.label != null) {
            intent.putExtra("label", alarm.label)
        }

        val pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0)


        return pendingIntent;
    }


}