package com.example.johnproject

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import java.util.*


class AlarmBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {

        val requestCode: Int? = intent?.getIntExtra("requestCode", 0)
        val timeToTrigger: Long = intent?.getLongExtra("time", 0)!!

        val snooze: Int? = intent?.getIntExtra("snooze", 0);
        val repeatOn: Int = intent?.getIntExtra("repeatOn", 0)!!
        val isRepeating: Boolean? = intent?.getBooleanExtra("isRepeating", false)
        val maxSnooze: Int? = intent?.getIntExtra("maxSnooze", 0)
        val label: String? = intent?.getStringExtra("label")


        Log.e("AlarmBroadcastReceiver", "request code: $requestCode , timeToTrigger: $timeToTrigger  ")

        val isAlarmValid = alarmValidation(repeatOn, timeToTrigger);

        Log.e("isAlarmValid", "$isAlarmValid ")
        
        if (isAlarmValid) {
            val i: Intent = createIntent(context, requestCode, timeToTrigger, snooze, repeatOn, isRepeating, maxSnooze, label)
            context!!.startActivity(i)
        }


    }


    private fun alarmValidation(repeatOn: Int, timeToTrigger: Long): Boolean {


        val oneMinute = 60000;
        val today: Calendar = Calendar.getInstance();
        val dayOfWeek: Int = today.get(Calendar.DAY_OF_WEEK)

        if (repeatOn == MyConstant.EVERYDAY ||
                repeatOn == MyConstant.ONETIME ||
                dayOfWeek == repeatOn) {

            val diff: Long = getDiff(today, timeToTrigger)

            return diff < oneMinute

        } else {
            return false
        }


    }

    private fun getDiff(today: Calendar, timeToTrigger: Long): Long {

        val yearOfToday: Int = today.get(Calendar.YEAR)
        val monthOfToday: Int = today.get(Calendar.MONTH)
        val dateOfToday: Int = today.get(Calendar.DATE)

        
        val alarm: Calendar = Calendar.getInstance()
        alarm.timeInMillis = timeToTrigger
        alarm.set(yearOfToday, monthOfToday, dateOfToday)


        val currentTime = today.timeInMillis
        val alarmTime = alarm.timeInMillis


        return currentTime - alarmTime

    }


    private fun createIntent(context: Context?, requestCode: Int?, timeToTrigger: Long, snooze: Int?, repeatOn: Int, isRepeating: Boolean?, maxSnooze: Int?, label: String?): Intent {
        val i: Intent = Intent(context, AlarmActivity::class.java)
        i.putExtra("requestCode", requestCode)
        i.putExtra("time", timeToTrigger)
        i.putExtra("snooze", snooze)
        i.putExtra("repeatOn", repeatOn)
        i.putExtra("isRepeating", isRepeating)
        i.putExtra("maxSnooze", maxSnooze)
        i.putExtra("label", label)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        return i
    }


}



