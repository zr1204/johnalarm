package com.example.johnproject

class Alarm {


    var id: String? = null;
    var label: String? = null;
    var milliseconds: Long? = null;
    var snooze: Int? = null;
    var maxSnooze: Int? = null;
    var repeat: List<Int>? = null;
    
    
    val recurringId = mutableListOf<Int>()



    constructor(_id: String,
                _label: String?,
                _milliseconds: Long,
                _snooze: Int,
                _maxSnooze: Int,
                _repeat: List<Int>) {

        this.id = _id
        this.label = _label
        this.milliseconds = _milliseconds
        this.snooze = _snooze
        this.maxSnooze = _maxSnooze
        this.repeat = _repeat

    }

}