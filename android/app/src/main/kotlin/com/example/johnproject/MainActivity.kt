package com.example.johnproject

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : FlutterActivity() {
    
    var customAlarmManager: CustomAlarmManager? = null

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);


        createNotificationChannel();
        
        customAlarmManager = CustomAlarmManager(this);


        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, "battery").setMethodCallHandler { call, result ->


            if (call.method == "scheduleAlarm1") {

                val alarm: Alarm = getAlarm(call);

                if (alarm.repeat!!.size == 7) { //Everyday

                    val requestCode: Int = (alarm.id!! + "7").toInt()
                    customAlarmManager!!.fireDailyAlarm(alarm, requestCode);

                } else if (alarm.repeat!!.isEmpty()) { //Only Once

                    val requestCode: Int = alarm.id!!.toInt();
                    customAlarmManager!!.fireOneTimeAlarm(alarm, requestCode);


                } else {

                    val isMonday: Boolean = alarm.repeat!!.contains(DayOfWeek.MONDAY.ordinal);
                    val isTuesday: Boolean = alarm.repeat!!.contains(DayOfWeek.TUESDAY.ordinal);
                    val isWednesday: Boolean = alarm.repeat!!.contains(DayOfWeek.WEDNESDAY.ordinal);
                    val isThursday: Boolean = alarm.repeat!!.contains(DayOfWeek.THURSDAY.ordinal);
                    val isFriday: Boolean = alarm.repeat!!.contains(DayOfWeek.FRIDAY.ordinal);
                    val isSaturday: Boolean = alarm.repeat!!.contains(DayOfWeek.SATURDAY.ordinal);
                    val isSunday: Boolean = alarm.repeat!!.contains(DayOfWeek.SUNDAY.ordinal);

                    if (isMonday) {

                        val requestCode: Int = (alarm.id!! + "1").toInt()
                        customAlarmManager!!.fireWeeklyAlarm(alarm, requestCode, DayOfWeek.MONDAY.ordinal);

                    }

                    if (isTuesday) {

                        val requestCode: Int = (alarm.id!! + "2").toInt()
                        customAlarmManager!!.fireWeeklyAlarm(alarm, requestCode, DayOfWeek.TUESDAY.ordinal);

                    }

                    if (isWednesday) {

                        val requestCode: Int = (alarm.id!! + "3").toInt()
                        customAlarmManager!!.fireWeeklyAlarm(alarm, requestCode, DayOfWeek.WEDNESDAY.ordinal);

                    }

                    if (isThursday) {

                        val requestCode: Int = (alarm.id!! + "4").toInt()
                        customAlarmManager!!.fireWeeklyAlarm(alarm, requestCode, DayOfWeek.THURSDAY.ordinal);

                    }

                    if (isFriday) {

                        val requestCode: Int = (alarm.id!! + "5").toInt()
                        customAlarmManager!!.fireWeeklyAlarm(alarm, requestCode, DayOfWeek.FRIDAY.ordinal);

                    }

                    if (isSaturday) {

                        val requestCode: Int = (alarm.id!! + "6").toInt()
                        customAlarmManager!!.fireWeeklyAlarm(alarm, requestCode, DayOfWeek.SATURDAY.ordinal);

                    }

                    if (isSunday) {

                        val requestCode: Int = (alarm.id!! + "0").toInt()
                        customAlarmManager!!.fireWeeklyAlarm(alarm, requestCode, DayOfWeek.SUNDAY.ordinal);

                    }
                }
            }
            if (call.method == "cancelAlarm1") {

                val id: String = call.argument("id")!!;
                val repeat: List<Int> = call.argument("repeat")!!;


                if (repeat.size == 7) { //Everyday

                    val requestCode: Int = (id + "7").toInt()
                    customAlarmManager!!.dismissAlarm(requestCode);


                } else if (repeat.isEmpty()) { //Only Once

                    val requestCode: Int = id.toInt()
                    customAlarmManager!!.dismissAlarm(requestCode);

                } else {

                    val isMonday: Boolean = repeat.contains(DayOfWeek.MONDAY.ordinal);
                    val isTuesday: Boolean = repeat.contains(DayOfWeek.TUESDAY.ordinal);
                    val isWednesday: Boolean = repeat.contains(DayOfWeek.WEDNESDAY.ordinal);
                    val isThursday: Boolean = repeat.contains(DayOfWeek.THURSDAY.ordinal);
                    val isFriday: Boolean = repeat.contains(DayOfWeek.FRIDAY.ordinal);
                    val isSaturday: Boolean = repeat.contains(DayOfWeek.SATURDAY.ordinal);
                    val isSunday: Boolean = repeat.contains(DayOfWeek.SUNDAY.ordinal);


                    if (isMonday) {
                        
                        val requestCode: Int = (id + "1").toInt()
                        customAlarmManager!!.dismissAlarm(requestCode);
                    }

                    if (isTuesday) {

                        val requestCode: Int = (id + "2").toInt()
                        customAlarmManager!!.dismissAlarm(requestCode);
                    }

                    if (isWednesday) {

                        val requestCode: Int = (id + "3").toInt()
                        customAlarmManager!!.dismissAlarm(requestCode);
                    }

                    if (isThursday) {

                        val requestCode: Int = (id + "4").toInt()
                        customAlarmManager!!.dismissAlarm(requestCode);
                    }

                    if (isFriday) {

                        val requestCode: Int = (id + "5").toInt()
                        customAlarmManager!!.dismissAlarm(requestCode);
                    }

                    if (isSaturday) {


                        val requestCode: Int = (id + "6").toInt()
                        customAlarmManager!!.dismissAlarm(requestCode);

                    }

                    if (isSunday) {

                        val requestCode: Int = (id + "0").toInt()

                        customAlarmManager!!.dismissAlarm(requestCode);
                    }


                }

            }

            if (call.method == "checkAlarmStatus") {

                val alarmId: String = call.argument("alarmId")!!;

                customAlarmManager!!.checkAlarmAvailable(alarmId.toInt());

            }
            
            if (call.method == "turnOnOnGoingNotification") {
                var milliSeconds: Long? = call.argument("milliseconds");


                turnOnNotification(milliSeconds!!)
                
            }

            if (call.method == "turnOffOnGoingNotification") {
                turnOffNotification()
            }


        }
    }

    private fun getAlarm(call: MethodCall): Alarm {
        val id: String = call.argument("id")!!;
        val label: String? = call.argument("label");
        val timeToTrigger: Long = call.argument("timeToTrigger")!!;
        val snooze: Int = call.argument("snooze")!!;
        val maxSnooze: Int = call.argument("maxSnooze")!!;
        val repeat: List<Int> = call.argument("repeat")!!;

        return Alarm(id, label, timeToTrigger, snooze, maxSnooze, repeat);
    }
    
    private fun turnOffNotification() {
        with(NotificationManagerCompat.from(this)) {
            cancel(1)
        }
    }

    private fun turnOnNotification(milliSeconds: Long) {
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        var builder = NotificationCompat.Builder(this, "channel_1")
                .setSmallIcon(R.drawable.ic_baby)
                .setContentText("[Next alarm] ${milliSecondToString(milliSeconds!!)}")
                .setContentTitle("John Android Alarm")
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.alarm1))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .build();

        with(NotificationManagerCompat.from(this)) {
            notify(1, builder)
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "channel_1"
            val descriptionText = "description of channel_1 "
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("channel_1", name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun milliSecondToString(millisecond: Long): String {

        val formatter: DateFormat = SimpleDateFormat("EEE hh:mm a") as DateFormat

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = millisecond
        return formatter.format(calendar.getTime());
    }

}
