package com.example.johnproject

import android.app.Application

object MyConstant {

    val EVERYDAY: Int = 99;
    val ONETIME: Int = 98;


}


internal enum class SnoozeOptions {
    OFF, ONE_MINUTE, FIVE_MINUTE, TEN_MINUTE, THIRTY_MINUTE
}

internal enum class MaxSnoozesOptions {
    UNLIMITED, ONE_TIME, TWO_TIMES, THREE_TIMES, FOUR_TIMES, FIVE_TIMES
}


internal enum class DayOfWeek {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
}