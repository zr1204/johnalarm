package com.example.johnproject

import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_second.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import android.media.Ringtone
import android.widget.Toast


class AlarmActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

       val alarm :Alarm =  getAlarmFromIntent(intent);


        setContentView(R.layout.activity_second)

        displayAlarmTime(alarm.milliseconds!!)

        setSnoozeBtnVisibility(alarm.snooze, alarm.maxSnooze)
        
        //Initialize ringtone
//        val ringtone = initRingtonePlayer()

        //Play alarm
//        ringtone!!.play()


        //Functionality of dismiss button
        btnDismiss.setOnClickListener {
//            ringtone.stop()
            finish()
        }


        
        //Functionality of snooze button
        btnSnooze.setOnClickListener {

            val alarmTime: Calendar = Calendar.getInstance()
            alarmTime.timeInMillis = alarm.milliseconds!!;

            val newAlarmTime: Calendar = Calendar.getInstance()
            newAlarmTime.set(Calendar.HOUR, alarmTime.get(Calendar.HOUR));
            newAlarmTime.set(Calendar.MINUTE, alarmTime.get(Calendar.MINUTE));
            newAlarmTime.set(Calendar.SECOND, 0);
            newAlarmTime.set(Calendar.MILLISECOND, 0);


            val snoozeDuration: Int = getSnoozeLength(alarm.snooze!!)
            newAlarmTime.add(Calendar.MILLISECOND,snoozeDuration)


            val id = "${newAlarmTime.get(Calendar.HOUR)}${newAlarmTime.get(Calendar.MINUTE)}"
            val label: String? =  alarm.label;
            val _milliseconds: Long =  newAlarmTime.timeInMillis;
            val _snooze: Int =  alarm.snooze!!;
            val _maxSnooze: Int =  alarm.maxSnooze!!;


            val snoozeAlarm =  Alarm(id ,label, _milliseconds, _snooze, _maxSnooze, emptyList())

            val customAlarmManager  = CustomAlarmManager(this);

            customAlarmManager.fireSnoozeAlarm(snoozeAlarm)

//            ringtone.stop()
            finish()

        }

    }



    private fun getAlarmFromIntent(intent: Intent): Alarm {

        val requestCode: Int = intent.getIntExtra("requestCode", 0)!!
        val timeToTrigger: Long = intent.getLongExtra("time", 0)!!
        val snooze: Int = intent.getIntExtra("snooze", 0)!!

        val maxSnooze: Int = intent.getIntExtra("maxSnooze", 0)!!
        val label: String? = intent.getStringExtra("label")


       return Alarm(requestCode.toString(),label,timeToTrigger,snooze,maxSnooze, emptyList());

    }


    private fun setSnoozeBtnVisibility(snooze: Int?, maxSnooze: Int?) {
        if (snooze == SnoozeOptions.OFF.ordinal || maxSnooze == 0) {
            btnSnooze.visibility = View.GONE;
        } else {
            btnSnooze.visibility = View.VISIBLE;
        }
    }

    private fun displayAlarmTime(timeToTrigger: Long) {
        tvTime.text = milliSecondToString(timeToTrigger)

        //Set Label Text
        if (intent.hasExtra("label")) {
            val label: String = intent.getStringExtra("label")
            tvLabel.text = label
        } else {
            tvLabel.visibility = View.INVISIBLE
        }
    }
    
    private fun initRingtonePlayer(): Ringtone? {
        var alarmUri: Uri? = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
        if (alarmUri == null) {
            alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        }
        val ringtone = RingtoneManager.getRingtone(this, alarmUri)
        return ringtone
    }

    private fun getSnoozeLength(snooze: Int): Int {
        return when (snooze) {
            SnoozeOptions.OFF.ordinal -> 0
            SnoozeOptions.ONE_MINUTE.ordinal -> (60000 * 1)
            SnoozeOptions.FIVE_MINUTE.ordinal -> (60000 * 5)
            SnoozeOptions.TEN_MINUTE.ordinal -> (60000 * 10)
            SnoozeOptions.THIRTY_MINUTE.ordinal -> (60000 * 30)
            else -> {
                0
            }
        }

    }
    
    private fun milliSecondToString(millisecond: Long): String {

        val formatter: DateFormat = SimpleDateFormat("EEE hh:mm a")

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = millisecond
        return formatter.format(calendar.getTime());
    }
}
