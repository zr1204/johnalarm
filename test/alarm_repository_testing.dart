import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive/hive.dart';
import 'package:johnproject/repository/alarm_repository_builder.dart';
import 'package:johnproject/repository/contract/alarm_repository.dart';
import 'package:johnproject/repository/model/alarm.dart';
import 'package:johnproject/unit_test/unit_test.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  const MethodChannel channel =
      MethodChannel('plugins.flutter.io/path_provider');
  channel.setMockMethodCallHandler((MethodCall methodCall) async {
    return ".";
  });

  String dir = (await getApplicationDocumentsDirectory()).path;

  Hive.init(dir);

  var alarm = Alarm((a) => a
    ..id = "1"
    ..label = "This is label"
    ..timeToTrigger = 1111111
    ..isActive = true
    ..snooze = 0
    ..maxSnooze = 0
    ..isRepeating = false
    ..repeat.addAll([1, 2, 3, 4, 5]));

  var alarm2 = Alarm((a) => a
    ..id = "2"
    ..label = "This is label2"
    ..timeToTrigger = 22222222
    ..isActive = true
    ..snooze = 0
    ..maxSnooze = 0
    ..isRepeating = false
    ..repeat.addAll([1, 2, 3, 4, 5]));

  List<Alarm> alarms = new List()..add(alarm)..add(alarm2);

  AlarmRepository alarmRepository = AlarmRepositoryBuilder.repository();

  test('Get Alarms', () async {
    List<Alarm> alarmList = await alarmRepository.getAlarms();
    print(alarmList.length);
  });

  test('Add Alarm', () async {
    await alarmRepository.addAlarm(alarms);
  });

  test('Edit Alarm', () async {
    await alarmRepository.editAlarm(alarms);
  });

  test('Remove Alarm', () async {
    await alarmRepository.removeAlarm(alarms);
  });
}
