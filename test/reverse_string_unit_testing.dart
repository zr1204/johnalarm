import 'package:flutter_test/flutter_test.dart';
import 'package:johnproject/unit_test/unit_test.dart';

void main() {
  test('String should be reversed', () {
    String initial = "Hello";
    String reversed = reverseString(initial);
    expect(reversed, "olleH");
  });

  test('caculate', () {
    expect(calculate(1, 2), 2);
  });
}
