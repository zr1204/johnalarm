import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:johnproject/module/alarm_controller.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:hive/hive.dart';

Future<void> main() async {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  AlarmController _controller;

  @override
  void initState() {
    super.initState();

    _controller =  new AlarmController(this);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            actions: <Widget>[],
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.music_note),
            onPressed: () async {
              _controller.playRingtone();
            },
          ),
          body: Container(
            width: double.infinity,
            height: double.infinity,
          ),
        ));
  }
}
